package clases;

public class Moto {
	private int id;
	private String marca;
	private String modelo;
	private int cilindrada;
	private int cv;
	private int cilindros;
	private boolean A2;

	//CONSTRUCTOR
	public Moto(int id) {
		this.id = id;
	}

	//METODO TOSTRING
	public String toString() {
		return "ID: " + id
				+ "\n Marca: " + marca
				+ "\n Modelo: " + modelo
				+ "\n Cilindrada: " + cilindrada
				+ "\n Caballos: " + cv
				+ "\n N� Cilindros: " + cilindros
				+ "\n A2?: " + A2;
				
				
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

	public int getCv() {
		return cv;
	}

	public void setCv(int cv) {
		this.cv = cv;
	}

	public int getCilindros() {
		return cilindros;
	}

	public void setCilindros(int cilindros) {
		this.cilindros = cilindros;
	}

	public boolean isA2() {
		return A2;
	}

	public void setA2(boolean a2) {
		A2 = a2;
	}
}
