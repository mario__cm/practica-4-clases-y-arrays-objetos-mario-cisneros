package clases;

public class Concesionario {

	private Moto[] motos;

	public void altaMoto(int id, String marca, String modelo, int cilindrada, int cv, int cilindros, boolean A2) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] == null) {
				motos[i] = new Moto(id);
				motos[i].setMarca(marca);
				motos[i].setModelo(modelo);
				motos[i].setCilindrada(cilindrada);
				motos[i].setCv(cv);
				motos[i].setCilindros(cilindros);
				motos[i].setA2(A2);
				break;
			}
		}
	}

	public Moto buscarMoto(int id) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				if (motos[i].getId() == (id)) {
					return motos[i];
				}
			}
		}

		return null;
	}

	public void eliminarMoto(int id) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				if (motos[i].getId() == (id)) {
					motos[i] = null;
				}
			}
		}
	}

	public void listarMotos() {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				System.out.println(motos[i]);
			}
		}
	}

	public void cambiarMarca(int id, String marca) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				if (motos[i].getId() == id) {
					motos[i].setMarca(marca);
				}
			}
		}
	}

	public void listarMarca(String marca) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				if (motos[i].getMarca().equals(marca)) {
					System.out.println(motos[i]);
				}
			}
		}
	}

	public Concesionario(int maxMotos) {
		this.motos = new Moto[maxMotos];
	}

	public Moto[] getMoto() {
		return motos;
	}

	public void setMoto(Moto[] moto) {
		this.motos = moto;
	}

}
