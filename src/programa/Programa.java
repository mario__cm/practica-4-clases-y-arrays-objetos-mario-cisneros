package programa;

import clases.Concesionario;

//import clases.Moto;

public class Programa {

	public static void main(String[] args) {
		int maxMotos = 5;

		System.out.println("Creamos el concesionario: ");
		Concesionario vialmotos = new Concesionario(maxMotos);
		System.out.println("Concesionario creado.");

		System.out.println("Doy de alta 5 motos:");
		vialmotos.altaMoto(0, "honda", "cbr", 600, 120, 4, false);
		vialmotos.altaMoto(1, "kawasaki", "er6n", 650, 73, 2, true);
		vialmotos.altaMoto(2, "yamaha", "r6", 600, 120, 4, false);
		vialmotos.altaMoto(3, "suzuki", "gsxr", 600, 120, 4, false);
		vialmotos.altaMoto(4, "suzuki", "adress", 49, 6, 1, true);

		System.out.println("Muestro las motos: ");
		// Listar las motos
		vialmotos.listarMotos();
		System.out.println("Busco Moto Suzuki Adress y la muestro");
		// Para buscar moto y mostrar
		System.out.println(vialmotos.buscarMoto(4));

		System.out.println("Cambio marca, de Suzuki Adress por motomala y lo mueestro");
		// Cambiar
		vialmotos.cambiarMarca(4, "motomala");
		vialmotos.listarMotos();
		System.out.println("Elimino moto 3: ");
		// Eliminiar moto
		vialmotos.eliminarMoto(3);
		vialmotos.listarMotos();
		// Listar por atributo
		System.out.println("Voy a mostrar por la marca honda: ");
		vialmotos.listarMarca("honda");
	}

}
